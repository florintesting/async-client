<?php

class AsyncClient
{
    /** @var array */
    protected $connections = [];

    /** @var array */
    private $shards = [];
    
    public function __construct()
    {
        // these should be in a config somewhere
        $this->shards = [
            [
                'host' => '0.0.0.0',
                'user' => 'user',
                'password' => 'password',
                'database' => 'database',
                'port' => '3306',
            ],
            [
                'host' => '0.0.0.1',
                'user' => 'user',
                'password' => 'password',
                'database' => 'database',
                'port' => '3306',
            ],
        ];

        $this->connect();
    }

    public function fetch(string $query): array
    {
        $collection = [];
        $links = $errors = $reject = [];

        for ($i = 0; $i < count($this->connections); $i++) {
            // set result mode MYSQLI_ASYNC, in order to async query
            $links[] = $this->connections[$i]->query($query, MYSQLI_ASYNC);
        }

        $total = count($links);
        $processed = 0;

        do {
            if ($this->checkDone($links) === false) {
                continue;
            }

            for ($i = 0; $i < $total; $i++) {
                if (mysqli_errno($links[$i])) {
                    throw new \AyncQueryException(mysqli_error($links[$i]), mysqli_errno($links[$i])); // not defined exception
                }

                // get results from async query
                if ($result = $links[$i]->reap_async_query()) {
                    while ($row = $result->fetch_assoc()) {
                        $collection[$i][] = $row;
                    }

                    // free the memory for each result
                    mysqli_free_result($result);
                }
                
                $processed++;
            }
        } while ($processed < $total);

        return $collection;
    }

    /**
     * Connect to a specific mysql server
     */
    public function attach(array $shard): AsyncClient
    {
        $connection = mysqli_connect(
            $shard['host'],
            $shard['user'],
            $shard['password'],
            $shard['database'],
            $shard['port']
        );

        if ($connection === false) {
            throw new \AyncConnectionException(mysqli_connect_error(), mysqli_connect_errno()); // not defined exception
        }

        $this->connections[] = $connection;

        return $this;
    }

    /**
     * Verify if the results can be read
     */
    protected function checkDone(array $links): bool
    {
        $errors = $reject = $links;
        
        // verify results ready, max 30 seconds
        if (! mysqli_poll($links, $errors, $reject, 30, 0)) {
            return false;
        }

        return true;
    }

    /**
     * Connect to config defined shards
     */
    protected function connect(): void
    {
        for ($i = 0; $i < count($this->shards); $i++) {
            $this->attach($this->shards[$i]);
        }
    }
}
